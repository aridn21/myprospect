import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const CarInfo = ({spec}) => {
  return (
    <View>
      <Text style={styles.carName}>
        Name: {spec.name}
      </Text>
      <Text>
        Brand: {spec.brand}
      </Text>
      <Text>
        Year: {spec.year}
      </Text>
      <Text>
        Engine: {spec.engine}
      </Text>
      <Text>
        Specification: {spec.specification}
      </Text>
      <Text>
        Price: IDR {spec.price}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  carName: {
    fontSize: 20
  },
});

export default CarInfo;
