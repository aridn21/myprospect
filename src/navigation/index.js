import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import DrawerNavigator from './navigator/DrawerNavigation';

export default function App(props) {
  return (
    <NavigationContainer>
        <DrawerNavigator />
    </NavigationContainer>
  );
}
