import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { About } from '../../Screen/About';
import App from '../../../App';

const Drawer = createDrawerNavigator();

export default function(props){
    return (
        <Drawer.Navigator>
            <Drawer.Screen name={'Home'} component={App}/>
            <Drawer.Screen name={'About'} component={About}/>
        </Drawer.Navigator>
    );
}