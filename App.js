/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
  Button,
  Alert
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import CarInfo from './src/components/atom/CarInfo';

const App = () => {
  //let carName = 'Honda Civic';
  //let brandName = 'Honda';

  // object untuk dikirim ke komponen carInfo
  let cars = [
    {
      image: "https://carsguide-res.cloudinary.com/image/upload/f_auto,fl_lossy,q_auto,t_cg_hero_large/v1/editorial/Honda_Civic_2020_grey_1.jpg",
      name: 'Honda Civic',
      brand: 'Honda',
      year: 2020,
      engine: 'xxx HP/ xxx TQ',
      specification: 'This car is really good-looking',
      price: 500000000
    },
    {
      image: "https://t1-cms-4.images.toyota-europe.com/toyotaone/euen/toyota-c-hr-2019-gallery-004-full_tcm-11-1776333.jpg",
      name: 'Toyota CHR',
      brand: 'Toyota',
      year: 2020,
      engine: 'xxx HP/ xxx TQ',
      specification: 'This car is really good-looking',
      price: 500000000
    },
    {
      image: "https://i.i-sgcm.com/new_cars/cars/12302/12302_m.jpg",
      name: 'Mazda CX-3',
      brand: 'Mazda',
      year: 2020,
      engine: 'xxx HP/ xxx TQ',
      specification: 'This car is really good-looking',
      price: 500000000
    }
  ];

  const [carSpec, setCarSpec] = useState(cars[0]);
  const [currentIndex, setCurrentIndex] = useState(0);

  const alertPrevButton = () => {
    if(currentIndex > 0){
      setCurrentIndex(currentIndex-1);
    }else{
      setCurrentIndex(cars.length-1);
      //currentIndex = cars.length-1;
    }

    setCarSpec(cars[currentIndex]);
    //Alert.alert('Current Index after Previous: ', currentIndex.toString());
  }

  const alertNextButton = () => {
    if(currentIndex < cars.length-1){
      setCurrentIndex(currentIndex+1);
    }else{
      setCurrentIndex(0);
    }

    setCarSpec(cars[currentIndex]);
    //Alert.alert('Current Index after Next: ', currentIndex.toString());
  }

  /*useEffect(() => {

  });*/

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Image
          source={{uri: carSpec.image}}
          style={{width: 420, height: 200}}
        />
        <ScrollView>
          <CarInfo spec={carSpec} />
          <View style={styles.container}>
            <View style={{flex:1}}>
              <Button title="Previous Car" onPress={() => {alertPrevButton()}} color="#000000"/>
            </View>
            <View style={{flex:1}}>
              <Button title="Next Car" onPress={() => {alertNextButton()}} />
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
  },
  carName: {
    fontSize: 20
  },
  halfColumn: {
    flex: 1,
    flexDirection: 'column'
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
